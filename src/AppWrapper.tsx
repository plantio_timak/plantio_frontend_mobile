import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import { NavigationContainer } from "@react-navigation/native";
import { StatusBar } from "expo-status-bar";

import WelcomeScreen from "./screens/WelcomeScreen";
import RegisterScreen from "./screens/RegisterScreen";
import LoginScreen from "./screens/LoginScreen";
import DashboardScreen from "./screens/DashboardScreen";
import Navigation from "./components/molecules/Navigation";
import {
  DASHBOARD_SCREEN_NAME,
  LOGIN_SCREEN_NAME,
  REGISTER_SCREEN_NAME,
  WELCOME_SCREEN_NAME,
} from "./constants/Screens";
import { headerHeight } from "./styles/Base";

const Drawer = createDrawerNavigator();

const AppWrapper: React.FC = () => {
  return (
    <NavigationContainer>
      <StatusBar style="light" />
      <Drawer.Navigator
        initialRouteName={WELCOME_SCREEN_NAME}
        drawerPosition="right"
        overlayColor="transparent"
        drawerStyle={{
          width: "60%",
          top: headerHeight,
        }}
        drawerContent={({ navigation }) => (
          <Navigation navigation={navigation} />
        )}
      >
        <Drawer.Screen
          name={WELCOME_SCREEN_NAME}
          component={WelcomeScreen}
          options={{ swipeEnabled: false }}
        />
        <Drawer.Screen
          name={REGISTER_SCREEN_NAME}
          component={RegisterScreen}
          options={{ swipeEnabled: false }}
        />
        <Drawer.Screen
          name={LOGIN_SCREEN_NAME}
          component={LoginScreen}
          options={{ swipeEnabled: false }}
        />
        <Drawer.Screen
          name={DASHBOARD_SCREEN_NAME}
          component={DashboardScreen}
        />
      </Drawer.Navigator>
    </NavigationContainer>
  );
};

export default AppWrapper;
