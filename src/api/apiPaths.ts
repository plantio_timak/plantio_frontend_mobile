import { PLANTS_MODULE } from "../constants/Services";

export const getRegisterUserApiPath = (): string => "/login/registration";
export const getLoginUserApiPath = (): string => "/login/authorization";

export const getRoomDataApiPath = (roomId: number): string =>
  `/watering/my/room/${roomId}?serviceType=PLANTS_MODULE&interval=WEEK`;
export const getRoomsApiPath = (): string =>
  `/watering/my/rooms/?serviceType=${PLANTS_MODULE}`;
export const getRoomPlantsDataApiPath = (roomId: number): string =>
  `/watering/my/plants/room/${roomId}?interval=WEEK`;
export const getRoomAddToUserApiPath = (): string => "/watering/my/room/add";
export const getSetLastUsedInstanceApiPath = (): string =>
  "/watering/my/rooms/last-used/set";
