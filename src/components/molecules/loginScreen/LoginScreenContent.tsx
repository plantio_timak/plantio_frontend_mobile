import React, { useEffect } from "react";
import { StyleSheet, View } from "react-native";
import { useForm } from "react-hook-form";
import { useNavigation } from "@react-navigation/native";
import { useDispatch, useSelector } from "react-redux";
import { isEmpty } from "lodash";

import Text from "../../atoms/Text";
import Input from "../../atoms/Input";
import Button from "../../atoms/Button";
import { dimensions } from "../../../styles/Base";
import { DASHBOARD_SCREEN_NAME } from "../../../constants/Screens";
import { LoginParams } from "../../../types/LoginScreenTypes";
import { StoreState } from "../../../types/StoreState";
import { getUserIsFetchingSelector } from "../../../redux/modules/user/reducer";
import { userLoginActionCreator } from "../../../redux/modules/user/actions";
import { onChangeHandler } from "../../../utils/Form";
import { FONT_SIZE_SMALL } from "../../../styles/Fonts";

const LoginScreenContent: React.FC = () => {
  const { register, handleSubmit, errors, setValue, getValues } = useForm<
    LoginParams
  >();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const isFetching = useSelector((state: StoreState) =>
    getUserIsFetchingSelector(state.user)
  );

  useEffect(() => {
    register(
      { name: "userName" },
      {
        required: "Username is required field",
      }
    );
    register(
      { name: "password" },
      {
        required: "Password is required field",
      }
    );
  }, [register, getValues]);

  const onSubmit = (values: LoginParams) => {
    const redirectToDashboard = () =>
      navigation.navigate(DASHBOARD_SCREEN_NAME);

    dispatch(userLoginActionCreator(values, redirectToDashboard));
  };

  return (
    <View style={styles.container}>
      <View>
        <Input
          label="Username"
          placeholder="type your name here"
          onChange={onChangeHandler(setValue, "userName")}
          error={errors?.userName?.message as string}
        />
        <Input
          label="Password"
          placeholder="type your password here"
          onChange={onChangeHandler(setValue, "password")}
          error={errors?.password?.message as string}
          isPassword
        />
      </View>
      <View style={styles.buttonContainer}>
        <Button
          label="Login"
          onPress={handleSubmit(onSubmit)}
          disabled={!isEmpty(errors) || isFetching}
        />
        <Text fontSize={FONT_SIZE_SMALL}>Forgot password?</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: "space-evenly",
    alignItems: "center",
    height: dimensions.fullHeightWithHeader,
  },
  buttonContainer: {
    alignItems: "center",
  },
});

export default LoginScreenContent;
