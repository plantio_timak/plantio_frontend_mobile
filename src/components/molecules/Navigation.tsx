import React from "react";
import { StyleSheet, View } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { DrawerNavigationHelpers } from "@react-navigation/drawer/lib/typescript/src/types";

import IconButton from "../atoms/IconButton";
import NavigationItem from "../atoms/NavigationItem";
import { ICON_SIZE_BIG, LOGOUT_ICON, USER_ICON } from "../../constants/Icon";
import { getUserSelector } from "../../redux/modules/user/reducer";
import { dimensions } from "../../styles/Base";
import {
  COLOR_BASE,
  COLOR_PRIMARY,
  COLOR_SECONDARY,
} from "../../styles/Colors";
import { StoreState } from "../../types/StoreState";
import { getNavigationItems } from "../../utils/Navigation";
import {
  userLogoutActionCreator,
  userSetSelectedServiceActionCreator,
} from "../../redux/modules/user/actions";
import { WELCOME_SCREEN_NAME } from "../../constants/Screens";
import { Service } from "../../types/Services";

type Props = {
  navigation: DrawerNavigationHelpers;
};

const Navigation: React.FC<Props> = ({ navigation }) => {
  const dispatch = useDispatch();

  const {
    userName,
    services,
    selectedService,
  } = useSelector((state: StoreState) => getUserSelector(state.user));

  const navItems = getNavigationItems(services);

  const handleLogout = () => {
    dispatch(userLogoutActionCreator());
    navigation.navigate(WELCOME_SCREEN_NAME);
  };

  return (
    <View style={styles.container}>
      <View>
        <View style={styles.userItem}>
          <IconButton
            iconName={USER_ICON}
            color={COLOR_SECONDARY}
            label={userName}
            onPress={() => {
              // TODO : nieco s userom
              return;
            }}
          />
        </View>
        {navItems?.map((item) => (
          <NavigationItem
            item={item}
            active={item.value === selectedService}
            onPress={(item) =>
              dispatch(userSetSelectedServiceActionCreator(item as Service))
            }
            key={item.value}
          />
        ))}
      </View>
      <View style={styles.logoutItem}>
        <IconButton
          iconName={LOGOUT_ICON}
          iconSize={ICON_SIZE_BIG}
          color={COLOR_SECONDARY}
          onPress={handleLogout}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: dimensions.fullHeightWithHeader,
    backgroundColor: COLOR_PRIMARY,
    justifyContent: "space-between",
  },
  userItem: {
    borderBottomWidth: 3,
    borderBottomColor: COLOR_BASE,
    alignItems: "center",
    padding: 10,
  },
  logoutItem: {
    alignItems: "center",
    padding: 30,
  },
});

export default Navigation;
