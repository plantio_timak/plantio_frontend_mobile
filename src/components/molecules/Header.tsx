import React from "react";
import { StyleSheet, View } from "react-native";
import { DrawerActions, useNavigation } from "@react-navigation/native";
import { useIsDrawerOpen } from "@react-navigation/drawer";

import IconButton from "../atoms/IconButton";
import { BACK_ICON, CLOSE_ICON, MENU_ICON } from "../../constants/Icon";
import { dimensions, headerHeight } from "../../styles/Base";
import { COLOR_PRIMARY, COLOR_SECONDARY } from "../../styles/Colors";
import { IconName } from "../../types/Icon";

type Props = {
  hasNavigation?: boolean;
};

const Header: React.FC<Props> = ({ hasNavigation }) => {
  const isDrawerOpen = useIsDrawerOpen();

  const toggleNav = () => navigation.dispatch(DrawerActions.toggleDrawer());

  const getNavIcon = (): IconName => {
    if (isDrawerOpen) {
      return CLOSE_ICON;
    }

    return MENU_ICON;
  };

  const navIcon = getNavIcon();

  const navigation = useNavigation();
  return (
    <View style={styles.container}>
      <IconButton
        iconName={BACK_ICON}
        label="PlantIO"
        onPress={navigation.goBack}
      />
      {hasNavigation && <IconButton iconName={navIcon} onPress={toggleNav} />}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: dimensions.fullWidth,
    height: headerHeight,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
    backgroundColor: COLOR_SECONDARY,
    borderBottomWidth: 5,
    borderBottomColor: COLOR_PRIMARY,
    paddingLeft: 10,
    paddingRight: 20,
    paddingTop: 20,
  },
  backButton: {
    flexDirection: "row",
    alignItems: "center",
  },
});

export default Header;
