import React, { useEffect } from "react";
import { StyleSheet, View } from "react-native";
import { useForm } from "react-hook-form";
import { useNavigation } from "@react-navigation/native";
import { isEmpty } from "lodash";
import { useDispatch, useSelector } from "react-redux";

import Input from "../../atoms/Input";
import Button from "../../atoms/Button";
import { dimensions } from "../../../styles/Base";
import { RegisterParams } from "../../../types/RegisterScreenTypes";
import { EMAIL_REGEX } from "../../../constants/Regex";
import { DASHBOARD_SCREEN_NAME } from "../../../constants/Screens";
import { onChangeHandler } from "../../../utils/Form";
import { userRegisterActionCreator } from "../../../redux/modules/user/actions";
import { StoreState } from "../../../types/StoreState";
import { getUserIsFetchingSelector } from "../../../redux/modules/user/reducer";

const RegisterScreenContent: React.FC = () => {
  const { register, handleSubmit, errors, setValue, getValues } = useForm<
    RegisterParams
  >();
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const isFetching = useSelector((state: StoreState) =>
    getUserIsFetchingSelector(state.user)
  );

  useEffect(() => {
    register(
      { name: "userName" },
      {
        required: "Username is required field",
        minLength: {
          value: 4,
          message: "Username should be at least 4 chars long",
        },
      }
    );
    register(
      { name: "email" },
      {
        required: "Email is required field",
        pattern: {
          value: EMAIL_REGEX,
          message: "Filled email is in wrong format",
        },
      }
    );
    register(
      { name: "password" },
      {
        required: "Password is required field",
        minLength: {
          value: 4,
          message: "Username should be at least 4 chars long",
        },
      }
    );
    register(
      { name: "confirmPassword" },
      {
        required: "Confirm password is required field",
        validate: (value) => {
          const values = getValues();
          const notMatchPassword = value !== values.password;

          if (notMatchPassword) {
            return "Password confirmation should match password";
          }

          return undefined;
        },
      }
    );
  }, [register, getValues]);

  const onSubmit = ({ userName, password, email }: RegisterParams) => {
    const filteredValues = {
      userName,
      password,
      email,
    };

    const redirectToDashboard = () =>
      navigation.navigate(DASHBOARD_SCREEN_NAME);

    dispatch(userRegisterActionCreator(filteredValues, redirectToDashboard));
  };

  return (
    <View style={styles.container}>
      <View>
        <Input
          label="Username"
          placeholder="type your name here"
          onChange={onChangeHandler(setValue, "userName")}
          error={errors?.userName?.message as string}
        />
        <Input
          label="Email"
          placeholder="type your email here"
          onChange={onChangeHandler(setValue, "email")}
          error={errors?.email?.message as string}
        />
        <Input
          label="Password"
          placeholder="type your password here"
          onChange={onChangeHandler(setValue, "password")}
          error={errors?.password?.message as string}
          isPassword
        />
        <Input
          label="Confirm password"
          placeholder="repeat password"
          onChange={onChangeHandler(setValue, "confirmPassword")}
          error={errors?.confirmPassword?.message as string}
          isPassword
        />
      </View>
      <Button
        label="Register"
        onPress={handleSubmit(onSubmit)}
        disabled={!isEmpty(errors) || isFetching}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: "space-evenly",
    alignItems: "center",
    height: dimensions.fullHeightWithHeader,
  },
});

export default RegisterScreenContent;
