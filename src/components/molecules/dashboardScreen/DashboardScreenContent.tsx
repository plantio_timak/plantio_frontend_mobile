import React, { useState } from "react";
import { StyleSheet, View } from "react-native";
import { useSelector } from "react-redux";

import Label from "../../atoms/Label";
import Text from "../../atoms/Text";
import StateWidget from "../../modules/plant/widget/StateWidget";
import PlantWidget from "../../modules/plant/widget/PlantWidget";
import { COLOR_SECONDARY } from "../../../styles/Colors";
import { FONT_SIZE_BIG } from "../../../styles/Fonts";
import Button from "../../atoms/Button";
import { StoreState } from "../../../types/StoreState";
import { getPlantsModuleSelectedRoomSelector } from "../../../redux/modules/plantsModule/reducer";
import ChangeRoomModal from "./ChangeRoomModal/ChangeRoomModal";

const DashboardScreenContent: React.FC = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const selectedRoom = useSelector((state: StoreState) =>
    getPlantsModuleSelectedRoomSelector(state.plantsModule)
  );

  return (
    <View>
      <View style={styles.title}>
        <Text fontSize={FONT_SIZE_BIG}>Dashboard</Text>
      </View>
      <View style={styles.dashboardHeader}>
        <Label text={selectedRoom?.name as string} />
        <View style={styles.actionButton}>
          <Button
            label="Change room"
            style="chooseAction"
            onPress={() => setIsModalVisible(true)}
          />
        </View>
      </View>
      <View>
        <ChangeRoomModal
          visible={isModalVisible}
          setIsModalVisible={setIsModalVisible}
        />
        <View style={styles.container}>
          <StateWidget />
          <PlantWidget />
          {/* <CalendarWidget /> */}
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: "space-evenly",
  },
  dashboardHeader: {
    marginVertical: 15,
    flexDirection: "row",
    alignItems: "center",
  },
  title: {
    paddingBottom: 15,
    borderBottomWidth: 3,
    borderBottomColor: COLOR_SECONDARY,
  },
  actionButton: {
    flex: 1,
    alignItems: "flex-end",
  },
});

export default DashboardScreenContent;
