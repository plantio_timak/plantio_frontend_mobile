import { AxiosError, AxiosResponse } from "axios";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { Alert } from "react-native";

import ChangeRoomContent from "./ChangeRoomContent";
import useCheckErrorCode from "../../../../hooks/useCheckErrorCode";
import { api } from "../../../../api/api";
import {
  getRoomDataApiPath,
  getRoomPlantsDataApiPath,
} from "../../../../api/apiPaths";
import { getUserTokenSelector } from "../../../../redux/modules/user/reducer";
import { RoomData } from "../../../../types/PlantModule";
import { StoreState } from "../../../../types/StoreState";
import { ROOM_NOT_EXIST_FOR_USER_ERROR_CODE } from "../../../../constants/ErrorCodes";
import { getPlantsModuleRoomsListSelector } from "../../../../redux/modules/plantsModule/reducer";
import { ListPickerItemType } from "../../../../types/ListPicker";

type Props = {
  selectedRoom: ListPickerItemType | null;
  setSelectedRoom: (room: ListPickerItemType | null) => void;
  selectedRoomData: RoomData;
  setSelectedRoomData: (room: RoomData | null) => void;
};

const SelectRoomFetcher: React.FC<Props> = ({
  selectedRoom,
  setSelectedRoom,
  selectedRoomData,
  setSelectedRoomData,
}) => {
  const token = useSelector((state: StoreState) =>
    getUserTokenSelector(state.user)
  );
  const rooms = useSelector((state: StoreState) =>
    getPlantsModuleRoomsListSelector(state.plantsModule)
  );
  const { checkErrorCode } = useCheckErrorCode();

  const [isRoomDataFetching, setRoomDataFetching] = useState(false);

  useEffect(() => {
    if (selectedRoom) {
      setRoomDataFetching(true);

      const fetchRoomData = api(token).get(
        getRoomDataApiPath(selectedRoom?.value as number)
      );
      const fetchRoomPlantsData = api(token).get(
        getRoomPlantsDataApiPath(selectedRoom?.value as number)
      );

      Promise.all([fetchRoomData, fetchRoomPlantsData])
        .then(
          ([
            { data: roomData },
            { data: roomPLantsData },
          ]: Array<AxiosResponse>) => {
            setRoomDataFetching(false);
            setSelectedRoomData({ ...roomData, ...roomPLantsData });
          }
        )
        .catch((error: AxiosError) => {
          setRoomDataFetching(false);
          const sessionErrorCode = error?.response?.data?.error;
          const [errorObject = null] = error?.response?.data?.errors || [];

          checkErrorCode(sessionErrorCode);

          if (errorObject) {
            const message =
              errorObject?.error === ROOM_NOT_EXIST_FOR_USER_ERROR_CODE
                ? "User is not assigned to selected room"
                : "Error occurred during fetching room data";
            Alert.alert(message)
          }

          setSelectedRoomData(null);
          setSelectedRoom(null);
        });
    }
  }, [selectedRoom, token, setSelectedRoom]);

  return (
    <ChangeRoomContent
      rooms={rooms}
      selectedRoom={selectedRoom}
      setSelectedRoom={setSelectedRoom}
      isSelectedRoomFetching={isRoomDataFetching}
      selectedRoomData={selectedRoomData}
    />
  );
};

export default SelectRoomFetcher;
