import React from "react";
import { StyleSheet, View } from "react-native";

import ListPicker from "../../../atoms/ListPicker";
import { Room, RoomData } from "../../../../types/PlantModule";
import { ListPickerItemType } from "../../../../types/ListPicker";

type Props = {
  rooms: Array<Room>;
  isSelectedRoomFetching: boolean;
  selectedRoom: ListPickerItemType | null;
  selectedRoomData: RoomData | null;
  setSelectedRoom: (room: ListPickerItemType) => void;
};

const SelectRoomContent: React.FC<Props> = ({
  rooms,
  selectedRoom,
  setSelectedRoom,
}) => {
  const getListPickerItems = (): Array<ListPickerItemType> =>
    rooms?.map((room) => ({
      value: room.id,
      label: room.name,
    }));

  const pickerItems = getListPickerItems();

  return (
    <View style={styles.changeRoomContainer}>
        <ListPicker
          items={pickerItems}
          selectedItem={selectedRoom}
          onItemPress={(value) => setSelectedRoom(value)}
        /> 
    </View>
  );
};

const styles = StyleSheet.create({
    changeRoomContainer: {
        display: "flex",
    },
  });

export default SelectRoomContent;
