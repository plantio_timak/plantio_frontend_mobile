import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Modal, ScrollView, StyleSheet, View } from "react-native";

import Button from "../../../atoms/Button";
import ChangeRoomFetcher from "./ChangeRoomFetcher";
import {
  plantsModuleFetchSelectedRoomDataSuccessActionCreator,
  plantsModuleSetSelectedRoomActionCreator,
} from "../../../../redux/modules/plantsModule/actions";
import { RoomData } from "../../../../types/PlantModule";
import { ListPickerItemType } from "../../../../types/ListPicker";
import { StoreState } from "../../../../types/StoreState";
import { getPlantsModuleSelectedRoomSelector } from "../../../../redux/modules/plantsModule/reducer";
import { getUserTokenSelector } from "../../../../redux/modules/user/reducer";
import { COLOR_PRIMARY, COLOR_SECONDARY } from "../../../../styles/Colors";
import Text from "../../../atoms/Text";

type Props = {
  visible: boolean;
  setIsModalVisible: (visible: boolean) => void;
};

const ChangeRoomModal: React.FC<Props> = ({
  visible,
  setIsModalVisible,
}) => {
  const dispatch = useDispatch();
  const token = useSelector((state: StoreState) =>
    getUserTokenSelector(state.user)
  );
  const lastSelectedRoom = useSelector((state: StoreState) =>
    getPlantsModuleSelectedRoomSelector(state.plantsModule)
  );
  const [selectedRoom, setSelectedRoom] = useState<ListPickerItemType | null>(
    null
  );
  const [selectedRoomData, setSelectedRoomData] = useState<RoomData | null>(
    null
  );

  useEffect(() => {
    if (lastSelectedRoom) {
      setSelectedRoom({
        value: lastSelectedRoom?.id,
        label: lastSelectedRoom?.name,
      });
    }
  }, [lastSelectedRoom]);

  const handleConfirmSelectedRoom = () => {
    dispatch(
      plantsModuleSetSelectedRoomActionCreator(
        selectedRoom?.value as number,
        token
      )
    );
    dispatch(
      plantsModuleFetchSelectedRoomDataSuccessActionCreator(selectedRoomData)
    );

    setIsModalVisible(false);
  };

  return (
    <Modal visible={visible}>
        <View style={styles.header}>
            <Text color={COLOR_PRIMARY}>Change room</Text>
        </View>
        <ScrollView style={{backgroundColor: COLOR_SECONDARY}}>
            <View style={styles.changeRoomContainer}>
            <ChangeRoomFetcher
                selectedRoom={selectedRoom}
                setSelectedRoom={setSelectedRoom}
                selectedRoomData={selectedRoomData as RoomData}
                setSelectedRoomData={setSelectedRoomData}
            />
            </View>
        </ScrollView>
        <View style={styles.footer}>
            <Button
            label="Confirm"
            onPress={handleConfirmSelectedRoom}
            style="secondary"
            disabled={!selectedRoom}
            />
            <Button
            label="Cancel"
            onPress={() => setIsModalVisible(false)}
            style="secondary"
            />
        </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
    changeRoomContainer: {
      alignItems: "center",
    },
    header: {
      backgroundColor: COLOR_SECONDARY,
      borderBottomWidth: 3,
      borderColor: COLOR_PRIMARY,
      padding: 15,
    },
    footer: {
      flexDirection: "row",
      backgroundColor: COLOR_SECONDARY,
      borderTopWidth: 3,
      borderColor: COLOR_PRIMARY,
      padding: 15,
      justifyContent: "center"
    },
  });
  
export default ChangeRoomModal;
