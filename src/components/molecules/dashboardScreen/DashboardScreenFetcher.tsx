import React, { useEffect } from "react";
import { ActivityIndicator } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import isEmpty from "lodash/isEmpty";

import DashboardScreenContent from "./DashboardScreenContent";
import {
  plantsModuleFetchRoomsListActionCreator,
  plantsModuleFetchSelectedRoomDataActionCreator,
} from "../../../redux/modules/plantsModule/actions";
import {
  getPlantsModuleIsRoomListFetchingSelector,
  getPlantsModuleIsSelectedRoomFetchingSelector,
  getPlantsModuleRoomsListSelector,
  getPlantsModuleSelectedRoomDataSelector,
  getPlantsModuleSelectedRoomSelector,
} from "../../../redux/modules/plantsModule/reducer";
import { StoreState } from "../../../types/StoreState";
import { getUserTokenSelector } from "../../../redux/modules/user/reducer";

const DashboardScreenFetcher: React.FC = () => {
  const dispatch = useDispatch();
  const token = useSelector((state: StoreState) =>
    getUserTokenSelector(state.user)
  );
  const rooms = useSelector((state: StoreState) =>
    getPlantsModuleRoomsListSelector(state.plantsModule)
  );
  const isRoomsListFetching = useSelector((state: StoreState) =>
    getPlantsModuleIsRoomListFetchingSelector(state.plantsModule)
  );
  const selectedRoom = useSelector((state: StoreState) =>
    getPlantsModuleSelectedRoomSelector(state.plantsModule)
  );
  const isSelectedRoomsFetching = useSelector((state: StoreState) =>
    getPlantsModuleIsSelectedRoomFetchingSelector(state.plantsModule)
  );
  const selectedRoomData = useSelector((state: StoreState) =>
    getPlantsModuleSelectedRoomDataSelector(state.plantsModule)
  );

  useEffect(() => {
    if (isEmpty(rooms)) {
      dispatch(plantsModuleFetchRoomsListActionCreator(token));
    }
  }, [dispatch, rooms, token]);

  useEffect(() => {
    if (selectedRoom && !selectedRoomData) {
      dispatch(
        plantsModuleFetchSelectedRoomDataActionCreator(selectedRoom?.id, token)
      );
    }
  }, [dispatch, selectedRoom, selectedRoomData, token]);

  if (isRoomsListFetching || isSelectedRoomsFetching) {
    return <ActivityIndicator />;
  }

  return <DashboardScreenContent />;
};

export default DashboardScreenFetcher;
