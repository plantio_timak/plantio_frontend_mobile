import React from "react";
import { StyleSheet, View } from "react-native";
import { useNavigation } from "@react-navigation/native";

import Text from "../../atoms/Text";
import Button from "../../atoms/Button";
import { dimensions } from "../../../styles/Base";
import { FONT_SIZE_TITLE } from "../../../styles/Fonts";
import {
  LOGIN_SCREEN_NAME,
  REGISTER_SCREEN_NAME,
} from "../../../constants/Screens";

const WelcomeScreenContent: React.FC = () => {
  const navigation = useNavigation();

  return (
    <View style={styles.container}>
      <Text fontSize={FONT_SIZE_TITLE}>PlantIO</Text>
      <View>
        <Button
          label="Login"
          onPress={() => navigation.navigate(LOGIN_SCREEN_NAME)}
        />
        <Button
          label="Register"
          onPress={() => navigation.navigate(REGISTER_SCREEN_NAME)}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    height: dimensions.fullHeight,
    justifyContent: "space-evenly",
    alignItems: "center",
  },
});

export default WelcomeScreenContent;
