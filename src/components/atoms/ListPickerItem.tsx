import React from "react";
import { StyleSheet, View } from "react-native";

import Button from "../atoms/Button";
import { ListPickerItemType } from "../../types/ListPicker";
import { COLOR_PRIMARY, COLOR_PRIMARY_DARK, COLOR_SECONDARY } from "../../styles/Colors";

type Props = {
  value: ListPickerItemType;
  onPress: (value: ListPickerItemType) => void;
  isSelected: boolean;
};

const ListPickerItem: React.FC<Props> = ({ value, onPress, isSelected }) => {
  const handlePress = () => {
    onPress(value);
  };

  const getClassName = () => {
    if (isSelected) {
      return styles.itemActive;
    }

    return styles.item;
  };

  const className = getClassName();

  return (
    <View style={className}>
        <Button 
        label={value?.label}
        onPress={handlePress}
        style="listPickerItem"
        />
    </View>

  );
};

const styles = StyleSheet.create({
    item: {
      borderWidth: 3,
      borderColor: COLOR_PRIMARY_DARK,
      color: COLOR_SECONDARY,
      flex: 1,
      backgroundColor: COLOR_PRIMARY,
    },
    itemActive: {
      borderWidth: 5,
      backgroundColor: COLOR_SECONDARY,
    },
  });

export default ListPickerItem;
