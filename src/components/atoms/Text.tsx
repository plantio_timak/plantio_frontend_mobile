import React from "react";
import { StyleSheet, Text as TextBase, TextStyle } from "react-native";

import { FONT_SIZE_NORMAL } from "../../styles/Fonts";
import { COLOR_SECONDARY } from "../../styles/Colors";
import { FontSize } from "../../types/Text";

type Props = {
  children: string | number | null;
  color?: string;
  fontSize?: FontSize;
  multiLine?: boolean;
  style?: TextStyle;
};

const Text: React.FC<Props> = ({
  children,
  multiLine,
  fontSize,
  color,
  style,
}) => {
  const numberOfLines = multiLine ? undefined : 1;

  return (
    <TextBase
      numberOfLines={numberOfLines}
      style={{ ...styles.text, fontSize, color, ...style }}
    >
      {children}
    </TextBase>
  );
};

const styles = StyleSheet.create({
  text: {
    fontFamily: "JosefinSans_400Regular",
    paddingTop: 5,
  },
});

Text.defaultProps = {
  multiLine: false,
  fontSize: FONT_SIZE_NORMAL,
  color: COLOR_SECONDARY,
};

export default Text;
