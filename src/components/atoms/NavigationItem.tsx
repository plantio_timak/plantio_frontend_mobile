import React from "react";
import { StyleSheet, View } from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";

import Text from "./Text";
import { COLOR_PRIMARY_LIGHT, COLOR_SECONDARY } from "../../styles/Colors";
import { FONT_SIZE_BIG } from "../../styles/Fonts";
import { Service } from "../../types/Services";

type Props = {
  item: { label: string; value: Service };
  active: boolean;
  onPress: (item: string) => void;
};

const NavigationItem: React.FC<Props> = ({ item, onPress, active }) => {
  const handlePress = () => {
    onPress(item.value);
  };

  return (
    <TouchableOpacity
      style={styles(active).container}
      activeOpacity={0.2}
      onPress={handlePress}
    >
      <Text fontSize={FONT_SIZE_BIG}>{item.label}</Text>
      {active && <View style={styles(active).tick} />}
    </TouchableOpacity>
  );
};

const styles = (active: boolean) =>
  StyleSheet.create({
    container: {
      flexDirection: "row",
      justifyContent: "center",
      alignItems: "center",
      height: 55,
      padding: 10,
      backgroundColor: active ? COLOR_PRIMARY_LIGHT : "transparent",
    },
    tick: {
      position: "absolute",
      right: 0,
      height: 55,
      width: 5,
      backgroundColor: COLOR_SECONDARY,
    },
  });

export default NavigationItem;
