import React from "react";
import { createIconSetFromIcoMoon } from "@expo/vector-icons";
import { TextStyle } from "react-native";

import icoMoonConfig from "../../../assets/icons/Config.json";
import { IconName, IconSize } from "../../types/Icon";
import { COLOR_PRIMARY } from "../../styles/Colors";

// eslint-disable-next-line
const expoAssetId = require("../../../assets/icons/Icons.ttf");

const IconBase = createIconSetFromIcoMoon(
  icoMoonConfig,
  "Icomoon",
  expoAssetId
);

type Props = {
  name: IconName;
  size: IconSize;
  color?: string;
  style?: TextStyle;
};

const Icon: React.FC<Props> = ({ color, style, name, size }: Props) => (
  <IconBase name={name as never} size={size} style={{ color, ...style }} />
);

Icon.defaultProps = {
  color: COLOR_PRIMARY,
};

export default Icon;
