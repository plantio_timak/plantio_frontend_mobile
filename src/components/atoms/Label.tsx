import React from "react";
import { StyleSheet, Text as TextBase, TextStyle } from "react-native";

import { FONT_SIZE_NORMAL } from "../../styles/Fonts";
import { COLOR_SECONDARY } from "../../styles/Colors";
import { FontSize, FontWeight } from "../../types/Text";
import { FONT_WEIGHT_NORMAL } from "../../styles/Weights";

type Props = {
  text: string;
  color?: string;
  fontSize?: FontSize;
  fontWeight?: FontWeight;
  multiLine?: boolean;
  style?: TextStyle;
};

const Label: React.FC<Props> = ({
  text,
  multiLine,
  fontSize,
  fontWeight,
  color,
  style,
}) => {
  const numberOfLines = multiLine ? undefined : 1;

  return (
    <TextBase
      numberOfLines={numberOfLines}
      style={{ ...styles.label, fontSize, fontWeight, color, ...style }}
    >
      {text}
    </TextBase>
  );
};

const styles = StyleSheet.create({
  label: {
    fontFamily: "JosefinSans_400Regular",
  },
});

Label.defaultProps = {
  multiLine: false,
  fontSize: FONT_SIZE_NORMAL,
  fontWeight: FONT_WEIGHT_NORMAL,
  color: COLOR_SECONDARY,
};

export default Label;
