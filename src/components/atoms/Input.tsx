import React from "react";
import { StyleSheet, TextInput, View } from "react-native";

import { dimensions } from "../../styles/Base";
import { COLOR_ERROR, COLOR_SECONDARY } from "../../styles/Colors";
import { FONT_SIZE_NORMAL, FONT_SIZE_SMALL } from "../../styles/Fonts";
import Text from "./Text";

type Props = {
  label: string;
  onChange: (text: string) => void;
  error?: string;
  placeholder?: string;
  isPassword?: boolean;
  defaultValue?: string | null;
};

// TODO : check placeholder color

const Input: React.FC<Props> = ({
  label,
  onChange,
  error,
  placeholder,
  isPassword,
  defaultValue,
}) => {
  const hasError = !!error;

  const color = hasError ? COLOR_ERROR : COLOR_SECONDARY;
  const contentType = isPassword ? "password" : "none";

  return (
    <View style={styles(hasError).container}>
      <Text fontSize={FONT_SIZE_SMALL} color={color}>
        {label}
      </Text>
      <TextInput
        placeholder={placeholder}
        style={{
          ...styles(hasError).input,
          color,
        }}
        onChangeText={onChange}
        textContentType={contentType}
        secureTextEntry={isPassword}
        autoCapitalize="none"
        defaultValue={defaultValue || ""}
      />
      {error && (
        <Text style={styles(hasError).error} color={COLOR_ERROR} multiLine>
          {error}
        </Text>
      )}
    </View>
  );
};

const styles = (hasError: boolean) =>
  StyleSheet.create({
    container: {
      paddingVertical: 5,
    },

    input: {
      fontSize: FONT_SIZE_NORMAL,
      fontFamily: "JosefinSans_400Regular",
      padding: 10,
      width: dimensions.fullWidth * 0.8,
      borderBottomWidth: 3,
      borderBottomColor: hasError ? COLOR_ERROR : COLOR_SECONDARY,
      color: hasError ? COLOR_ERROR : COLOR_SECONDARY,
    },

    error: {
      maxWidth: dimensions.fullWidth * 0.8,
      fontSize: FONT_SIZE_SMALL,
      marginTop: 5,
    },
  });

export default Input;
