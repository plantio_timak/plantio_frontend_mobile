import React from "react";
import { StyleSheet, TouchableOpacity } from "react-native";

import Text from "./Text";
import {
  COLOR_BASE,
  COLOR_DISABLED,
  COLOR_PRIMARY,
  COLOR_SECONDARY,
} from "../../styles/Colors";
import { FONT_SIZE_BIG, FONT_SIZE_SMALL } from "../../styles/Fonts";
import { dimensions } from "../../styles/Base";

type Props = {
  label: string;
  onPress: () => void;
  style?: string;
  disabled?: boolean;
};

const Button: React.FC<Props> = ({ label, onPress, style, disabled }) => {
  const getButtonStyle = () => {
    switch (style) {
      case "secondary":
        return styles(disabled).buttonSecondary;

      case "listPickerItem":
        return styles(disabled).buttonListPickerItem;

      case "chooseAction":
        return styles(disabled).buttonChooseAction;

      default:
        return styles(disabled).buttonPrimary;
    }
  };

  const getTextColor = () => {
    if (!disabled) {
      switch (style) {
        case "secondary":
          return COLOR_PRIMARY;

        default:
          return COLOR_SECONDARY;
      }
    } else {
      return COLOR_BASE;
    }
  };

  const getTextSize = () => {
    switch (style) {
      case "chooseAction":
        return FONT_SIZE_SMALL;

      default:
        return FONT_SIZE_BIG;
    }
  };

  const buttonStyle = getButtonStyle();
  const buttonTextColor = getTextColor();
  const buttonTextSize = getTextSize();

  return (
    <TouchableOpacity
      activeOpacity={0.2}
      style={buttonStyle}
      onPress={onPress}
      disabled={disabled}
    >
      <Text fontSize={buttonTextSize} color={buttonTextColor}>
        {label}
      </Text>
    </TouchableOpacity>
  );
};

const styles = (disabled: boolean | undefined) =>
  StyleSheet.create({
    buttonPrimary: {
      width: dimensions.fullWidth * 0.8,
      height: 50,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: disabled ? COLOR_DISABLED : COLOR_PRIMARY,
      marginBottom: 15,
    },
    buttonChooseAction: {
      width: dimensions.fullWidth * 0.5,
      height: 40,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: disabled ? COLOR_DISABLED : COLOR_PRIMARY,
      fontSize: FONT_SIZE_SMALL,
    },
    buttonSmallLabel: {
      fontSize: FONT_SIZE_SMALL,
    },
    buttonSecondary: {
      width: dimensions.fullWidth * 0.4,
      alignItems: "center",
      justifyContent: "center",
      borderWidth: 3,
      borderColor: COLOR_PRIMARY,
      backgroundColor: COLOR_SECONDARY,
    },
    buttonListPickerItem: {
      width: dimensions.fullWidth * 0.8,
      height: 50,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: disabled ? COLOR_DISABLED : COLOR_PRIMARY,
    },
    buttonAutowidth: {
      width: dimensions.fullWidth,
    },
    disabled: {
      backgroundColor: COLOR_DISABLED,
    },
  });

export default Button;
