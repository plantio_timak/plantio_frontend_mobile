import React from "react";
import { TouchableOpacity } from "react-native-gesture-handler";
import { StyleSheet } from "react-native";

import Icon from "./Icon";
import Text from "./Text";
import { ICON_SIZE_NORMAL } from "../../constants/Icon";
import { IconName, IconSize } from "../../types/Icon";
import { COLOR_PRIMARY } from "../../styles/Colors";
import { FONT_SIZE_BIG } from "../../styles/Fonts";

type Props = {
  iconName: IconName;
  iconSize?: IconSize;
  color?: string;
  label?: string | null;
  onPress: () => void;
};

const IconButton: React.FC<Props> = ({
  iconName,
  iconSize,
  onPress,
  label,
  color,
}) => (
  <TouchableOpacity
    activeOpacity={0.2}
    onPress={onPress}
    style={styles.container}
  >
    <Icon name={iconName} size={iconSize as IconSize} color={color} />
    {label && (
      <Text fontSize={FONT_SIZE_BIG} color={color} style={styles.label}>
        {label}
      </Text>
    )}
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    alignItems: "center",
  },
  label: {
    paddingLeft: 5,
  },
});

IconButton.defaultProps = {
  color: COLOR_PRIMARY,
  iconSize: ICON_SIZE_NORMAL,
};

export default IconButton;
