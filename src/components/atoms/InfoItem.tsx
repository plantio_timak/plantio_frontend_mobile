import React from "react";
import { StyleSheet, View } from "react-native";

import Label from "./Label";
import { FONT_SIZE_NORMAL } from "../../styles/Fonts";
import { FONT_WEIGHT_BOLD } from "../../styles/Weights";

type Props = {
  label: string;
  value: string | number | undefined;
  unit?: string;
};

const InfoItem: React.FC<Props> = ({ value, label, unit }) => {
  const roundValue = (value: string | number) => {
    if (typeof value === "number") {
      return value.toFixed(2);
    }

    return value;
  };

  const formatValue = (): string | number => {
    if (!value && value !== 0) {
      return "";
    }

    const roundedValue = roundValue(value);

    if (unit) {
      return `${roundedValue} ${unit}`;
    }

    return roundedValue;
  };

  const formatedValue = formatValue();

  return (
    <View style={styles.infoItem}>
      <Label text={label} fontSize={FONT_SIZE_NORMAL} />
      <Label
        text={formatedValue as string}
        fontSize={FONT_SIZE_NORMAL}
        fontWeight={FONT_WEIGHT_BOLD}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  infoItem: {
    flexDirection: "row",
    display: "flex",
    justifyContent: "space-between",
    paddingVertical: 10,
  },
});

export default InfoItem;
