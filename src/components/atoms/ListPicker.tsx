import React from "react";
import { StyleSheet, View } from "react-native";

import ListPickerItem from "./ListPickerItem";
import { ListPickerItemType } from "../../types/ListPicker";
import { COLOR_PRIMARY_DARK } from "../../styles/Colors";

type Props = {
  items: Array<ListPickerItemType>;
  selectedItem: ListPickerItemType | null;
  onItemPress: (value: ListPickerItemType) => void;
};

const ListPicker: React.FC<Props> = ({ items, selectedItem, onItemPress }) => (
  <View style={styles.container}>
    {items?.map((item) => (
      <ListPickerItem
        value={item}
        key={item.value}
        onPress={onItemPress}
        isSelected={item.value === selectedItem?.value}
      />
    ))}
  </View>
);

const styles = StyleSheet.create({
    container: {
      borderTopWidth: 1,
      borderColor: COLOR_PRIMARY_DARK,
    },
  });

export default ListPicker;
