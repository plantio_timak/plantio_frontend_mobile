import React from "react";
import { StyleSheet, View } from "react-native";
import { useSelector } from "react-redux";

import Widget from "../../../templates/widget/Widget";
import Label from "../../../atoms/Label";
import { getWateringModeLabel } from "../../../../utils/PlantsModule";
import { getPlantsModuleSelectedRoomPlantsDataSelector } from "../../../../redux/modules/plantsModule/reducer";
import { StoreState } from "../../../../types/StoreState";
import { PlantData } from "../../../../types/PlantModule";
import { COLOR_BASE, COLOR_PRIMARY } from "../../../../styles/Colors";
import { FONT_SIZE_SMALL } from "../../../../styles/Fonts";
import { FONT_WEIGHT_BOLD } from "../../../../styles/Weights";

const PlantWidget: React.FC = () => {
  const plants = useSelector((state: StoreState) =>
    getPlantsModuleSelectedRoomPlantsDataSelector(state.plantsModule)
  );

  const renderPlantContent = (plant: PlantData) => {
    const [lastMeasuredValue] = plant?.plantValues || [];

    const getValueAndUnit = (value: number, unit: string) => {
      if (value || value === 0) {
        return `${value.toFixed(2)} ${unit}`;
      }

      return "";
    };

    return (
      <View key={plant.id} style={styles.plantContainer}>
        <Label text={`${plant.name} - ${plant.plant}`} />
        <View style={styles.plantFieldsContainer}>
          <View style={styles.plantFieldsLabelsContainer}>
            <Label
              style={styles.plantFieldLabel}
              fontSize={FONT_SIZE_SMALL}
              text="Water mode"
            />
            <Label
              style={styles.plantFieldLabel}
              fontSize={FONT_SIZE_SMALL}
              text="Soil humidity"
            />
            <Label
              style={styles.plantFieldLabel}
              fontSize={FONT_SIZE_SMALL}
              text="Soil temp"
            />
          </View>
          <View style={styles.plantFieldsValuesContainer}>
            <Label
              style={styles.plantFieldLabel}
              fontSize={FONT_SIZE_SMALL}
              fontWeight={FONT_WEIGHT_BOLD}
              text={getWateringModeLabel(plant?.mode?.plantMode)}
            />
            <Label
              style={styles.plantFieldLabel}
              fontSize={FONT_SIZE_SMALL}
              fontWeight={FONT_WEIGHT_BOLD}
              text={getValueAndUnit(lastMeasuredValue?.values?.humidity, "%")}
            />
            <Label
              style={styles.plantFieldLabel}
              fontSize={FONT_SIZE_SMALL}
              fontWeight={FONT_WEIGHT_BOLD}
              text={getValueAndUnit(
                lastMeasuredValue?.values?.temperature,
                "°C"
              )}
            />
          </View>
        </View>
      </View>
    );
  };

  return (
    <View>
      <Widget title="Plants">{plants.map(renderPlantContent)}</Widget>
    </View>
  );
};

const styles = StyleSheet.create({
  plantContainer: {
    backgroundColor: COLOR_BASE,
    marginVertical: 10,
    padding: 15,
  },
  plantFieldsContainer: {
    marginTop: 10,
    borderTopColor: COLOR_PRIMARY,
    borderTopWidth: 1,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  plantFieldsLabelsContainer: {
    paddingTop: 10,
    width: "50%",
    borderRightColor: COLOR_PRIMARY,
    borderRightWidth: 1,
  },
  plantFieldLabel: {
    paddingVertical: 8.75,
  },
  plantFieldsValuesContainer: {
    paddingTop: 10,
    width: "50%",
    alignItems: "flex-end",
  },
});

export default PlantWidget;
