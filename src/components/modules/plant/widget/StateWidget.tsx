import React from "react";
import { View } from "react-native";
import { useSelector } from "react-redux";
import isNil from "lodash/isNil";

import InfoItem from "../../../atoms/InfoItem";
import Widget from "../../../templates/widget/Widget";
import { getPlantsModuleSelectedRoomDataSelector } from "../../../../redux/modules/plantsModule/reducer";
import { StoreState } from "../../../../types/StoreState";

const StateWidget: React.FC = () => {
  const roomData = useSelector((state: StoreState) =>
    getPlantsModuleSelectedRoomDataSelector(state.plantsModule)
  );
  const [lastMeasuredValue] = roomData?.data || [];

  const getWaterCapacityValue = () => {
    if (!isNil(lastMeasuredValue?.values?.waterCapacity)) {
      if (lastMeasuredValue?.values?.waterCapacity) {
        return "Full";
      }

      return "Empty";
    }

    return "";
  };

  return (
    <View>
      <Widget title="State">
        <InfoItem
          label="Temperature"
          value={lastMeasuredValue?.values?.temperature}
          unit="°C"
        />
        <InfoItem
          label="Humidity"
          value={lastMeasuredValue?.values?.humidity}
          unit="%"
        />
        <InfoItem
          label="Light intensity"
          value={lastMeasuredValue?.values?.lightIntensity}
        />
        <InfoItem label="Water state" value={getWaterCapacityValue()} />
      </Widget>
    </View>
  );
};

export default StateWidget;
