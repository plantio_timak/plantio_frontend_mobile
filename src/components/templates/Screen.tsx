import React from "react";
import { ScrollView, StyleSheet, View } from "react-native";

import Header from "../molecules/Header";
import { COLOR_BASE } from "../../styles/Colors";
import { dimensions } from "../../styles/Base";

type Props = {
  hasHeader?: boolean;
  hasNavigation?: boolean;
};

const Screen: React.FC<Props> = ({ children, hasHeader, hasNavigation }) => (
  <View style={styles.container}>
    {hasHeader && <Header hasNavigation={hasNavigation} />}
    <ScrollView style={styles.content}>{children}</ScrollView>
  </View>
);

const styles = StyleSheet.create({
  container: {
    height: dimensions.fullHeight,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: COLOR_BASE,
  },
  content: {
    paddingTop: 15,
  },
});

Screen.defaultProps = {
  hasHeader: true,
  hasNavigation: true,
};

export default Screen;
