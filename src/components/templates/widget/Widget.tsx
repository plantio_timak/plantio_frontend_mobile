import React from "react";
import { StyleSheet, View } from "react-native";

import Text from "../../atoms/Text";
import {
  COLOR_PRIMARY_DARK,
  COLOR_PRIMARY_LIGHTER,
  COLOR_SECONDARY,
} from "../../../styles/Colors";
import { dimensions } from "../../../styles/Base";
import { FONT_SIZE_BIG } from "../../../styles/Fonts";

type Props = {
  title: string;
  hasNavigation?: boolean;
};

const Widget: React.FC<Props> = ({ title, children }) => (
  <View style={styles.container}>
    <View style={styles.content}>
      <View style={styles.title}>
        <Text fontSize={FONT_SIZE_BIG}>{title}</Text>
      </View>
      {children}
    </View>
  </View>
);

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    width: dimensions.fullWidth * 0.875,
    backgroundColor: COLOR_PRIMARY_LIGHTER,
    borderColor: COLOR_PRIMARY_DARK,
    borderWidth: 1,
    paddingVertical: 15,
    paddingHorizontal: 30,
    marginBottom: 30,
  },
  content: {
    flexDirection: "column",
    flex: 1,
  },
  title: {
    paddingBottom: 15,
    marginBottom: 15,
    borderBottomWidth: 3,
    borderBottomColor: COLOR_SECONDARY,
  },
});

export default Widget;
