export const BUTTON_NORMAL = "normal";
export const BUTTON_SMALL = "small";

export const BUTTON_PRIMARY = "primary";
export const BUTTON_SECONDARY = "secondary";
