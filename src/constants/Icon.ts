// icon names
export const BACK_ICON = "back";
export const CLOSE_ICON = "close";
export const MENU_ICON = "menu";
export const USER_ICON = "user";
export const LOGOUT_ICON = "logout";

export const ICON_SIZE_SMALL = 20;
export const ICON_SIZE_NORMAL = 30;
export const ICON_SIZE_BIG = 40;
