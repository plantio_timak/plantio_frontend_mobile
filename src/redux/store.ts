import AsyncStorage from "@react-native-community/async-storage";
import createSagaMiddleware from "redux-saga";
import { AnyAction, applyMiddleware, createStore } from "redux";
import { persistReducer, persistStore } from "redux-persist";
import { Reducer } from "react";

import rootReducer from "./reducer";
import rootSaga from "./sagas";

const persistConfig = {
  key: "root",
  storage: AsyncStorage,
  whitelist: ["user"],
};

const sagaMiddleware = createSagaMiddleware();

const persistedReducer = persistReducer(
  persistConfig,
  rootReducer as Reducer<unknown, AnyAction>
);

const store = createStore(persistedReducer, applyMiddleware(sagaMiddleware));

const persistor = persistStore(store);

sagaMiddleware.run(rootSaga);

export { store, persistor };
