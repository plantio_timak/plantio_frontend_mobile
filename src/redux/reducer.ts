import { AnyAction, CombinedState, combineReducers } from "redux";

import { StoreState } from "../types/StoreState";
import user from "./modules/user/reducer";
import plantsModule from "./modules/plantsModule/reducer"

// import stores from "./modules/stores/reducer";
// import weights from "./modules/weights/reducer";
// import { USER_LOGOUT_ACTION } from "./modules/user/actions";
// TODO : add flush store on logout
const appReducer = combineReducers<StoreState>({
  user,plantsModule
});

const rootReducer = (
  state: StoreState,
  action: AnyAction
): CombinedState<StoreState> => {
  //   if (action.type === USER_LOGOUT_ACTION) {
  //     return (state = undefined as CombinedState<undefined>);
  //   }
  return appReducer(state, action);
};

export default rootReducer;
