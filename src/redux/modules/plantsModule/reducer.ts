import { PlantData, Room, RoomData } from "../../../types/PlantModule";
import {
  PLANTS_MODULE_FETCH_ROOMS_LIST_ACTION,
  PLANTS_MODULE_FETCH_ROOMS_LIST_FAILURE_ACTION,
  PLANTS_MODULE_FETCH_ROOMS_LIST_SUCCESS_ACTION,
  PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_ACTION,
  PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_FAILURE_ACTION,
  PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_SUCCESS_ACTION,
  PLANTS_MODULE_SET_SELECTED_ROOM_ACTION,
  PLANTS_MODULE_SET_SELECTED_ROOM_SUCCESS_ACTION,
  PlantsModuleAction,
} from "./actions";

export type PlantsModuleState = {
  isRoomsFetching: boolean;
  isSelectedRoomDataFetching: boolean;
  isSelectedRoomSaveFetching: boolean;
  roomsFetchError: string | null;
  selectedRoomDataFetchError: string | null;
  rooms: Array<Room>;
  selectedRoom: number | null;
  selectedRoomData: RoomData | null;
};

export const initialState = {
  isRoomsFetching: false,
  isSelectedRoomDataFetching: false,
  isSelectedRoomSaveFetching: false,
  roomsFetchError: null,
  selectedRoomDataFetchError: null,
  rooms: [],
  selectedRoom: null,
  selectedRoomData: null,
};

const plantsModuleReducer = (
  state: PlantsModuleState = initialState,
  action: PlantsModuleAction
): PlantsModuleState => {
  switch (action.type) {
    case PLANTS_MODULE_SET_SELECTED_ROOM_ACTION: {
      return { ...state, isSelectedRoomSaveFetching: true };
    }
    case PLANTS_MODULE_SET_SELECTED_ROOM_SUCCESS_ACTION: {
      return {
        ...state,
        isSelectedRoomSaveFetching: false,
        selectedRoom: action.payload.roomId,
      };
    }
    case PLANTS_MODULE_FETCH_ROOMS_LIST_ACTION: {
      return { ...state, isRoomsFetching: true };
    }
    case PLANTS_MODULE_FETCH_ROOMS_LIST_SUCCESS_ACTION: {
      return { ...state, isRoomsFetching: false, rooms: action.payload.rooms };
    }
    case PLANTS_MODULE_FETCH_ROOMS_LIST_FAILURE_ACTION: {
      return {
        ...state,
        isRoomsFetching: false,
        roomsFetchError: action.payload.error,
      };
    }
    case PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_ACTION: {
      return {
        ...state,
        isSelectedRoomDataFetching: true,
      };
    }
    case PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_SUCCESS_ACTION: {
      return {
        ...state,
        isSelectedRoomDataFetching: false,
        selectedRoomData: action.payload.roomData,
      };
    }
    case PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_FAILURE_ACTION: {
      return {
        ...state,
        isSelectedRoomDataFetching: false,
        selectedRoomDataFetchError: action.payload.error,
      };
    }
    default: {
      return state;
    }
  }
};

export const getPlantsModuleSelectedRoomSelector = (
  state: PlantsModuleState
): Room | null =>
  state?.rooms?.find((room) => room.id === state?.selectedRoom) || null;
export const getPlantsModuleSelectedRoomDataSelector = (
  state: PlantsModuleState
): RoomData | null => state?.selectedRoomData;
export const getPlantsModuleSelectedRoomPlantsDataSelector = (
  state: PlantsModuleState
): Array<PlantData> => state?.selectedRoomData?.plants || [];
export const getPlantsModuleRoomsListSelector = (
  state: PlantsModuleState
): Array<Room> => state?.rooms;
export const getPlantsModuleIsRoomListFetchingSelector = (
  state: PlantsModuleState
): boolean => state.isRoomsFetching;
export const getPlantsModuleIsSelectedRoomFetchingSelector = (
  state: PlantsModuleState
): boolean => state.isSelectedRoomDataFetching;

export default plantsModuleReducer;
