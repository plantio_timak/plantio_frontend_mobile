import { AxiosResponse } from "axios";
import {
  AllEffect,
  CallEffect,
  PutEffect,
  all,
  call,
  put,
  takeEvery,
} from "redux-saga/effects";

import { api } from "../../../api/api";
import {
  getRoomDataApiPath,
  getRoomPlantsDataApiPath,
  getRoomsApiPath,
  getSetLastUsedInstanceApiPath,
} from "../../../api/apiPaths";
import {
  PLANTS_MODULE_FETCH_ROOMS_LIST_ACTION,
  PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_ACTION,
  PLANTS_MODULE_SET_SELECTED_ROOM_ACTION,
  PlantsModuleAction,
  PlantsModuleFetchRoomsListAction,
  PlantsModuleFetchSelectedRoomDataAction,
  PlantsModuleSetSelectedRoomAction,
  plantsModuleFetchRoomsListFailureActionCreator,
  plantsModuleFetchRoomsListSuccessActionCreator,
  plantsModuleFetchSelectedRoomDataFailureActionCreator,
  plantsModuleFetchSelectedRoomDataSuccessActionCreator,
  plantsModuleSetSelectedRoomSuccessActionCreator,
} from "./actions";

function* fetchRoomsList(action: PlantsModuleFetchRoomsListAction) {
  const { token } = action.payload;

  try {
    const response = yield call(api(token).get, getRoomsApiPath());
    yield put(
      plantsModuleFetchRoomsListSuccessActionCreator(response.data?.userRooms)
    );
  } catch (error) {
    const message = "Failed to fetch user rooms";

    yield put(plantsModuleFetchRoomsListFailureActionCreator(message));
  }
}

function* fetchSelectedRoomData(
  action: PlantsModuleFetchSelectedRoomDataAction
): Generator<
  AllEffect<CallEffect<unknown>> | PutEffect<PlantsModuleAction>,
  void,
  [AxiosResponse, AxiosResponse]
> {
  const { token, roomId } = action.payload;

  try {
    const [roomDataResponse, roomPlantsDataResponse] = yield all([
      call(api(token).get, getRoomDataApiPath(roomId)),
      call(api(token).get, getRoomPlantsDataApiPath(roomId)),
    ]);

    yield put(
      plantsModuleFetchSelectedRoomDataSuccessActionCreator({
        data: roomDataResponse?.data,
        ...roomPlantsDataResponse?.data,
      })
    );
  } catch (error) {
    const message = "Failed to fetch selected room data";

    yield put(plantsModuleFetchSelectedRoomDataFailureActionCreator(message));
  }
}

function* setSelectedRoom(action: PlantsModuleSetSelectedRoomAction) {
  const { token, roomId } = action.payload;
  try {
    yield call(api(token).put, getSetLastUsedInstanceApiPath(), {
      lastUsedServiceInstanceId: roomId,
    });

    yield put(plantsModuleSetSelectedRoomSuccessActionCreator(roomId));
  } catch (error) {
    yield put(plantsModuleSetSelectedRoomSuccessActionCreator(roomId));
  }
}

function* fetchRoomsListSaga() {
  yield takeEvery(PLANTS_MODULE_FETCH_ROOMS_LIST_ACTION, fetchRoomsList);
}

function* fetchSelectedRoomDataSaga() {
  yield takeEvery(
    PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_ACTION,
    fetchSelectedRoomData
  );
}

function* setSelectedRoomSaga() {
  yield takeEvery(PLANTS_MODULE_SET_SELECTED_ROOM_ACTION, setSelectedRoom);
}

export default [
  fetchRoomsListSaga(),
  fetchSelectedRoomDataSaga(),
  setSelectedRoomSaga(),
];
