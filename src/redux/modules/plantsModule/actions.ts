import { Room, RoomData } from "../../../types/PlantModule";

export const PLANTS_MODULE_SET_SELECTED_ROOM_ACTION =
  "PLANTS_MODULE_SET_SELECTED_ROOM_ACTION";
export const PLANTS_MODULE_SET_SELECTED_ROOM_SUCCESS_ACTION =
  "PLANTS_MODULE_SET_SELECTED_ROOM_SUCCESS_ACTION";
export const PLANTS_MODULE_FETCH_ROOMS_LIST_ACTION =
  "PLANTS_MODULE_FETCH_ROOMS_LIST_ACTION";
export const PLANTS_MODULE_FETCH_ROOMS_LIST_SUCCESS_ACTION =
  "PLANTS_MODULE_FETCH_ROOMS_LIST_SUCCESS_ACTION";
export const PLANTS_MODULE_FETCH_ROOMS_LIST_FAILURE_ACTION =
  "PLANTS_MODULE_FETCH_ROOMS_LIST_FAILURE_ACTION";
export const PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_ACTION =
  "PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_ACTION";
export const PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_SUCCESS_ACTION =
  "PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_SUCCESS_ACTION";
export const PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_FAILURE_ACTION =
  "PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_FAILURE_ACTION";

export type PlantsModuleSetSelectedRoomAction = {
  type: typeof PLANTS_MODULE_SET_SELECTED_ROOM_ACTION;
  payload: {
    token: string | null;
    roomId: number | null;
  };
};
export type PlantsModuleSetSelectedRoomSuccessAction = {
  type: typeof PLANTS_MODULE_SET_SELECTED_ROOM_SUCCESS_ACTION;
  payload: {
    roomId: number | null;
  };
};

export type PlantsModuleFetchRoomsListAction = {
  type: typeof PLANTS_MODULE_FETCH_ROOMS_LIST_ACTION;
  payload: {
    token: string | null;
  };
};

export type PlantsModuleFetchRoomsListSuccessAction = {
  type: typeof PLANTS_MODULE_FETCH_ROOMS_LIST_SUCCESS_ACTION;
  payload: {
    rooms: Array<Room>;
  };
};

export type PlantsModuleFetchRoomsListFailureAction = {
  type: typeof PLANTS_MODULE_FETCH_ROOMS_LIST_FAILURE_ACTION;
  payload: {
    error: string;
  };
};

export type PlantsModuleFetchSelectedRoomDataAction = {
  type: typeof PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_ACTION;
  payload: {
    roomId: number;
    token: string | null;
  };
};

export type PlantsModuleFetchSelectedRoomDataSuccessAction = {
  type: typeof PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_SUCCESS_ACTION;
  payload: {
    roomData: RoomData | null;
  };
};

export type PlantsModuleFetchSelectedRoomDataFailureAction = {
  type: typeof PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_FAILURE_ACTION;
  payload: {
    error: string;
  };
};

export type PlantsModuleAction =
  | PlantsModuleSetSelectedRoomAction
  | PlantsModuleSetSelectedRoomSuccessAction
  | PlantsModuleFetchRoomsListAction
  | PlantsModuleFetchRoomsListSuccessAction
  | PlantsModuleFetchRoomsListFailureAction
  | PlantsModuleFetchSelectedRoomDataAction
  | PlantsModuleFetchSelectedRoomDataSuccessAction
  | PlantsModuleFetchSelectedRoomDataFailureAction;

export const plantsModuleSetSelectedRoomActionCreator = (
  roomId: number | null,
  token: string | null,
): PlantsModuleAction => ({
  type: PLANTS_MODULE_SET_SELECTED_ROOM_ACTION,
  payload: { roomId, token},
});
export const plantsModuleSetSelectedRoomSuccessActionCreator = (
  roomId: number | null
): PlantsModuleAction => ({
  type: PLANTS_MODULE_SET_SELECTED_ROOM_SUCCESS_ACTION,
  payload: { roomId },
});

export const plantsModuleFetchRoomsListActionCreator = (
  token: string | null,
): PlantsModuleAction => ({
  type: PLANTS_MODULE_FETCH_ROOMS_LIST_ACTION,
  payload: { token },
});
export const plantsModuleFetchRoomsListSuccessActionCreator = (
  rooms: Array<Room>
): PlantsModuleAction => ({
  type: PLANTS_MODULE_FETCH_ROOMS_LIST_SUCCESS_ACTION,
  payload: { rooms },
});
export const plantsModuleFetchRoomsListFailureActionCreator = (
  error: string
): PlantsModuleAction => ({
  type: PLANTS_MODULE_FETCH_ROOMS_LIST_FAILURE_ACTION,
  payload: { error },
});
export const plantsModuleFetchSelectedRoomDataActionCreator = (
  roomId: number,
  token: string | null,
): PlantsModuleAction => ({
  type: PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_ACTION,
  payload: { roomId, token },
});
export const plantsModuleFetchSelectedRoomDataSuccessActionCreator = (
  roomData: RoomData | null
): PlantsModuleAction => ({
  type: PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_SUCCESS_ACTION,
  payload: { roomData },
});
export const plantsModuleFetchSelectedRoomDataFailureActionCreator = (
  error: string
): PlantsModuleAction => ({
  type: PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_FAILURE_ACTION,
  payload: { error },
});
