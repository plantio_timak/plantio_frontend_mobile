import { Alert } from "react-native";
import { call, put, takeEvery } from "redux-saga/effects";

import {
  USER_LOGIN_ACTION,
  USER_REGISTER_ACTION,
  UserLoginAction,
  UserRegisterAction,
  userLoginFailureActionCreator,
  userLoginSuccessActionCreator,
  userRegisterFailureActionCreator,
  userRegisterSuccessActionCreator,
} from "./actions";
import { api } from "../../../api/api";
import {
  getLoginUserApiPath,
  getRegisterUserApiPath,
} from "../../../api/apiPaths";
import { FIELD_INVALID_ERROR_CODE } from "../../../constants/ErrorCodes";
import { PLANTS_MODULE } from "../../../constants/Services";
import { plantsModuleSetSelectedRoomSuccessActionCreator } from "../plantsModule/actions";


function* userRegister(action: UserRegisterAction) {
  const { payload } = action;
  const { params, redirectToDashboard} = payload;

  try {
    const response = yield call(api().post, getRegisterUserApiPath(), params);

    yield put(userRegisterSuccessActionCreator(response.data));

    Alert.alert("User successfully registered and logged in");
    redirectToDashboard();
  } catch (error) {
    const [errorObject = null] = error?.response?.data?.errors || [];

    const message =
      errorObject?.error === FIELD_INVALID_ERROR_CODE
        ? "User with selected username already exists"
        : "Error occurred during registration";

    yield put(userRegisterFailureActionCreator(message));

  }
}

function* userLogin(action: UserLoginAction) {
  const { payload } = action;
  const { params, redirectToDashboard } = payload;

  try {
    const response = yield call(api().post, getLoginUserApiPath(), params);

    const selectedService = response.data?.lastUsedService;

    const formatedData = {
      ...response.data,
      selectedService,
    };

    yield put(userLoginSuccessActionCreator(formatedData));

    if (selectedService === PLANTS_MODULE) {
      yield put(
        plantsModuleSetSelectedRoomSuccessActionCreator(
          response.data?.lastUsedServiceInstance
        )
      );
    }

    redirectToDashboard();
  } catch (error) {
    const [errorObject = null] = error?.response?.data?.errors || [];

    const message =
      errorObject?.error === FIELD_INVALID_ERROR_CODE
        ? "Wrong username or password"
        : "Error occurred during logging in";

    yield put(userLoginFailureActionCreator(message));

  }
}

function* userRegisterSaga() {
  yield takeEvery(USER_REGISTER_ACTION, userRegister);
}

function* userLoginSaga() {
  yield takeEvery(USER_LOGIN_ACTION, userLogin);
}

export default [userRegisterSaga(), userLoginSaga()];
