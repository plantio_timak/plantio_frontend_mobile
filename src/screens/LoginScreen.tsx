import React from "react";

import LoginScreenContent from "../components/molecules/loginScreen/LoginScreenContent";
import Screen from "../components/templates/Screen";

const LoginScreen: React.FC = () => (
  <Screen hasNavigation={false}>
    <LoginScreenContent />
  </Screen>
);

export default LoginScreen;
