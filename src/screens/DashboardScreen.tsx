import React from "react";

import DashboardScreenFetcher from "../components/molecules/dashboardScreen/DashboardScreenFetcher";
import Screen from "../components/templates/Screen";

const DashboardScreen: React.FC = () => (
  <Screen>
    <DashboardScreenFetcher/>
  </Screen>
);

export default DashboardScreen;
