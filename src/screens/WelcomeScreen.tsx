import React from "react";

import Screen from "../components/templates/Screen";
import WelcomeScreenContent from "../components/molecules/welcomeScreen/WelcomeScreenContent";

const WelcomeScreen: React.FC = () => (
  <Screen hasHeader={false}>
    <WelcomeScreenContent />
  </Screen>
);

export default WelcomeScreen;
