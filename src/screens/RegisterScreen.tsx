import React from "react";

import RegisterScreenContent from "../components/molecules/registerScreen/RegisterScreenContent";
import Screen from "../components/templates/Screen";

const RegisterScreen: React.FC = () => (
  <Screen hasNavigation={false}>
    <RegisterScreenContent />
  </Screen>
);

export default RegisterScreen;
