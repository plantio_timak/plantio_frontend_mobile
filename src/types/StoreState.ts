import { PlantsModuleState } from "../redux/modules/plantsModule/reducer";
import { UserState } from "../redux/modules/user/reducer";

export type StoreState = {
  user: UserState;
  plantsModule: PlantsModuleState;
};
