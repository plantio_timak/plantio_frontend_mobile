import {
  PLANT_MODE_INTERACTIVE,
  PLANT_MODE_TIME,
} from "../constants/PlantsModule";

type RoomSensorInfo = {
  humidity: number;
  lightIntensity: number;
  temperature: number;
  waterCapacity: number;
};

type PlantSensorInfo = {
  humidity: number;
  temperature: number;
};

export type PlantMode = typeof PLANT_MODE_TIME | typeof PLANT_MODE_INTERACTIVE;

export type WateringMode = {
  id: number;
  name: string;
};

export type PlantData = {
  id: number;
  name: string;
  plant: string;
  mode: {
    plantMode: PlantMode;
    time?: string;
  };
  wateringMode?: WateringMode;
  plantValues: Array<PlantDataApi>;
};

type PlantDataApi = {
  time: string;
  topic: string;
  values: PlantSensorInfo;
};

export type RoomDataApi = {
  time: number;
  topic: string;
  values: RoomSensorInfo;
};

export type RoomPlantsDataApi = {
  plants: Array<PlantData>;
};

export type Room = {
  id: number;
  name: string;
};

export type RoomData = { data: Array<RoomDataApi> } & RoomPlantsDataApi;
