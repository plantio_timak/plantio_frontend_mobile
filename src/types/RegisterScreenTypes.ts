export type RegisterParams = {
  userName: string;
  email: string;
  password: string;
  confirmPassword?: string;
};
