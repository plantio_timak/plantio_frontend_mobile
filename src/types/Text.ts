import {
  FONT_SIZE_BIG,
  FONT_SIZE_NORMAL,
  FONT_SIZE_SMALL,
  FONT_SIZE_TITLE,
} from "../styles/Fonts";
import {
  FONT_WEIGHT_BOLD,
  FONT_WEIGHT_NORMAL,
} from "../styles/Weights"

export type FontSize =
  | typeof FONT_SIZE_SMALL
  | typeof FONT_SIZE_NORMAL
  | typeof FONT_SIZE_BIG
  | typeof FONT_SIZE_TITLE;

export type FontWeight =
  | typeof FONT_WEIGHT_BOLD
  | typeof FONT_WEIGHT_NORMAL;