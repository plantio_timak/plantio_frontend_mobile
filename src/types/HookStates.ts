export type UseCheckErrorCodeState = {
  checkErrorCode: (errorCode: string) => void;
};
