import {
  BACK_ICON,
  CLOSE_ICON,
  ICON_SIZE_BIG,
  ICON_SIZE_NORMAL,
  ICON_SIZE_SMALL,
  LOGOUT_ICON,
  MENU_ICON,
  USER_ICON,
} from "../constants/Icon";

export type IconName =
  | typeof BACK_ICON
  | typeof MENU_ICON
  | typeof CLOSE_ICON
  | typeof USER_ICON
  | typeof LOGOUT_ICON;

export type IconSize =
  | typeof ICON_SIZE_SMALL
  | typeof ICON_SIZE_NORMAL
  | typeof ICON_SIZE_BIG;
