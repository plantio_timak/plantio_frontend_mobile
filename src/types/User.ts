import { Service } from "./Services";

export type User = {
  userName: string | null;
  email: string | null;
  token: string | null;
  services: Array<Service>;
  selectedService: Service | null;
};
