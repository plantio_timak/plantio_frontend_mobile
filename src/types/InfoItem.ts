import { INFO_ITEM_NORMAL, INFO_ITEM_SMALL } from "../constants/InfoItem";

export type InfoItemSize = typeof INFO_ITEM_SMALL | typeof INFO_ITEM_NORMAL;
