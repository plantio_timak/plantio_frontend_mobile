import { useDispatch } from "react-redux";

import { TOKEN_INVALID_ERROR_CODE } from "../constants/ErrorCodes";
import { userLogoutActionCreator } from "../redux/modules/user/actions";
import { UseCheckErrorCodeState } from "../types/HookStates";

const useCheckErrorCode = (): UseCheckErrorCodeState => {
  const dispatch = useDispatch();

  const checkErrorCode = (errorCode: string): void => {
    if (errorCode === TOKEN_INVALID_ERROR_CODE) {
      setTimeout(() => dispatch(userLogoutActionCreator()), 5000);
    }
  };

  return {
    checkErrorCode,
  };
};

export default useCheckErrorCode;
