import {
    PLANT_MODE_INTERACTIVE,
    PLANT_MODE_TIME,
  } from "../constants/PlantsModule";
  import { PlantMode } from "../types/PlantModule";
  
  export const getWateringModeLabel = (mode: PlantMode): string => {
    switch (mode) {
      case PLANT_MODE_INTERACTIVE:
        return "Adaptive";
      case PLANT_MODE_TIME:
        return "Time";
      default:
        return "";
    }
  };
  