export const onChangeHandler = <T>(
  setValue: (id: T, value: string, conf: { shouldValidate: boolean }) => void,
  name: T
) => (value: string): void => setValue(name, value, { shouldValidate: true });
