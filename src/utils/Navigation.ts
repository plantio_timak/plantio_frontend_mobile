import { PLANTS_MODULE } from "../constants/Services";
import { Service } from "../types/Services";

const getModuleLabel = (module: string): string => {
  switch (module) {
    case PLANTS_MODULE:
      return "plants module";
    default:
      return "";
  }
};

export const getNavigationItems = (
  services: Array<Service>
): Array<{ label: string; value: Service }> =>
  services?.map((service) => ({
    label: getModuleLabel(service),
    value: service,
  }));
