import { Dimensions } from "react-native";

export const headerHeight = 100;

export const dimensions = {
  fullHeight: Dimensions.get("window").height,
  fullHeightWithHeader: Dimensions.get("window").height - headerHeight,
  fullWidth: Dimensions.get("window").width,
};
