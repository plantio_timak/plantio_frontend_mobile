export const FONT_SIZE_SMALLER = 15;
export const FONT_SIZE_SMALL = 20;
export const FONT_SIZE_NORMAL = 25;
export const FONT_SIZE_BIG = 30;
export const FONT_SIZE_BIGGER = 35;
export const FONT_SIZE_TITLE = 60;
