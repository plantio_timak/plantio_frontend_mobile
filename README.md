This project was created with [Expo](https://docs.expo.io/).

## Project Setup

Needed prerequisities to run this project:

- **Node.js** - JavaScript runtime - version >= 12.18 - [docs](https://nodejs.org/en/)
- **yarn** - JavaScript package manager - version >= 1.22 - [docs](https://yarnpkg.com/)
- **Expo CLI** - main interface for Expo tools - version >= 3.27 - [docs](https://docs.expo.io/get-started/installation/)

## Available Scripts

In the project directory, you can run:

### `yarn`

Installs all needed packages.

**Note: this script needs to executed after clone of repository!**

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:19002](http://localhost:19002) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

### `yarn android`

Opens app in the Expo client on a connected Android device.

### `yarn ios`

Opens app in the Expo client in a currently running iOS simulator on your computer.

### `yarn lint`

Formats code with [ESlint](https://eslint.org/).

### `yarn lint:fix`

Formats code with [ESlint](https://eslint.org/) and fix fixable problems.

### `prettier:write`

Prettifies code with prettier code formatter.

## Commiting

We are using [husky](https://typicode.github.io/husky/#/) commit hooks to check code formatting and commit messages.

Correct commit message looks like: `git commit -m "<type>: <message>"`

In message is written what changes were done with that commit. In type we specify type of done work:

Types of available types:

- build
- ci
- chore
- docs
- feat
- fix
- perf
- refactor
- revert
- style
- test

## Learn More

You can learn more in the [React Native documentation](https://reactnative.dev/).

To learn React, check out the [React documentation](https://reactjs.org/).
