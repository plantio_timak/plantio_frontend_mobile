import React, { ReactElement } from "react";
import { AppLoading } from "expo";
import {
  JosefinSans_400Regular,
  useFonts,
} from "@expo-google-fonts/josefin-sans";
import { PersistGate } from "redux-persist/integration/react";
import { Provider } from "react-redux";

import AppWrapper from "./src/AppWrapper";
import { persistor, store } from "./src/redux/store";

const App = (): ReactElement => {
  const [fontsLoaded] = useFonts({ JosefinSans_400Regular });

  if (!fontsLoaded) {
    return <AppLoading />;
  }

  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <AppWrapper />
      </PersistGate>
    </Provider>
  );
};

export default App;
